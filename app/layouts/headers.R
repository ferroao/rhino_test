box::use(
  shiny[tagList, div],
)

box::use(
  shiny.fluent[IconButton.shinyInput, Text],
)

#' @export
header_right <- tagList(
  IconButton.shinyInput(
    "button", iconProps = list("iconName" = "CollapseMenu")),
  div(Text(variant = "xLarge", "example")
      , class = "title"
      , style= "align: center; white-space:nowrap;"
  )
)
