# R packages
box::use(
  shiny[moduleServer, NS, renderText, tags, textOutput, tagList, div,
        fluidPage,reactiveValuesToList,observe,
        observeEvent],
)

box::use(
  shinyjs[useShinyjs]
)

box::use(
  shiny.router[...]
)

box::use(
  app / layouts / sidebar[...],
  app / layouts / headers[...],
  app / layouts / footers[...],
  app / layouts / pages[...],
)

box::use(
  app / view / submit,
)

router <- purrr::lift(make_router)(pages_menu)

#' @export
ui <- function(id) {
  # ns <- NS(id) # see router and pages_menu
  fluidPage(
  useShinyjs(),
  shiny::tags$body(
    dir = "ltr",
    div(
      class = "grid-container",
      div(class = "header_right", header_right),
      div(class = "sidenav", sidebar, id = "sidebar_id"),
      div(class = "main", router$ui),
      div(class = "footer", footer)
    )
  )
)
}

#
#   server
#
#' @export
server <- function(id) {
  moduleServer(id, function(input, output, session) {
    router$server(input, output, session)

  #
  #   inputs
  #

  vars_submit <- submit$server("mod_submit")

  observeEvent(input$button, {
    shinyjs::toggle("sidebar_id")
  })

})}
