#
# submit module
#
box::use(
  shiny[
    moduleServer,
    NS,
    tags,
    tagList,
    div,
    uiOutput,
    conditionalPanel,
    renderUI,
    br,
    observeEvent,
    reactiveValues,
    outputOptions,
    observe,
    reactiveValuesToList
    ],
  shiny.semantic[
    split_layout,
    multiple_checkbox,
    selectInput
  ]
)

box::use(
  app / objects / global[...],
)

#' @export
ui <- function(id) {
  ns <- NS(id)
  split_layout(style = "background:#FFFFFF;",
  cell_widths = c("30%","5%","30%","5%","30%"),
    tagList(

  div(style=styleTop,
    selectInput(ns("region_input"),"Region",c("America"=1,"Africa"=2)
                )
  )
  ,div(
       conditionalPanel(condition = "input.region_input == 2",
                        ns = ns,
                        uiOutput(ns("africaCountrySelect"))
       )
  )
  ,div(
       conditionalPanel(condition = "input.region_input == 1",
                         ns = ns,
                         uiOutput(ns("centerSelect"))
       )
  )
  ,div(
       conditionalPanel(condition = "input.region_input == 2",
                         ns = ns,
                         uiOutput(ns("centerSelect2"))
       )
  )

)
)

}

#
# server
#
#' @export
server <- function(id) {
  moduleServer(id, function(input, output, session) {
    ns <- session$ns

    observeEvent(input$region_input,{
      list_of_inputs <<- reactiveValuesToList(input)
      # print(list_of_inputs)
      cat("inputs: ", input$region_input,"\n")
    })

    rv<-reactiveValues(notReady=TRUE, subtaghide=0)

    output$africaCountrySelect <- renderUI({
      div(style=styleTop
        , selectInput(ns("africa_country_input")
                      , "Country"
                      , c("Senegal","Egypt")
        )
      )
    })

    outputOptions(output, "africaCountrySelect", suspendWhenHidden=F)

    output$centerSelect <- renderUI({
      div(style=styleTop,
        selectInput(ns("center_input"),"State"
                      ,c("state1","state2")
                      ,selected = "state1"
        )
      )
    })

    # output$centerSelect2 <- renderUI({
    #   validate(
    #     need(
    #       try(input$region_input),""
    #     )
    #   )
    #   div(style=styleTop,
    #     selectInput(ns("center_input_2"),"Location"
    #                 ,unique(afr_locations[
    #                   which(
    #                     afr_locations$country_name ==
    #                            input$africa_country_input),]$location_name)
    #     )
    #   )
    # })


})}
